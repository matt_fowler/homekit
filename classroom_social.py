import sys, requests, json, os
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

mist_token = os.environ.get("MIST_TOKEN")
org_id = os.environ.get("ORG_ID")
mist_sites = "/api/v1/orgs/"+ org_id +"/sites"
mist_api = "https://api.mist.com"

def get_content(uri, api):
	# GET request to Mist based on the uri you provide.
	try:
		head = {'Authorization': 'token {}'.format(mist_token)}
		request = requests.get(url = api+uri, headers = head, verify=False, timeout=2)
		return request.json()
	except requests.exceptions.RequestException as e:
		return "[]"

def post_content(uri, payload, api):
	# GET request to Mist based on the uri you provide.
	try:
		head = {'Authorization': 'token {}'.format(mist_token)}
		request = requests.post(url = api+uri, headers = head, json = payload, verify=False, timeout=2)
		return request.json()
	except requests.exceptions.RequestException as e:
		return "[]"

def wxlan_api(room, token, action, mist_sites, mist_api):
	for site in get_content(mist_sites, mist_api):
		site_id = site["id"]
		check_tag = get_content("/api/v1/sites/"+ site_id +"/wxtags", mist_api)
		for i in check_tag:
			if i['name'] == room and i['match'] == 'ap_id':
				check_rule = get_content("/api/v1/sites/"+ site_id +"/wxrules", mist_api)
				for r in check_rule:
					if 'name' in r:
						if (r['name'] == room):
							if action == 'block':
								if r['enabled'] == False:
									payload = {
										"name": room,
										"order": 1,
										"src_wxtags": [i['id']],
										"dst_allow_wxtags": [],
										"dst_deny_wxtags": [],
										"blocked_apps": ["all-social"],
										"action": "block",
										"enabled": True
										}
									uri = "/api/v1/sites/"+ site_id +"/wxrules/"+ r['id']
									post = post_content(uri, payload, mist_api)
									if post['enabled'] == True:
										wxlanresult = "Social media rule was found is now blocked for room "+ room
										return wxlanresult
									else:
										wxlanresult = "Something went wrong error:1"
										return wxlanresult
								elif r['enabled'] == True:
									wxlanresult = "Social media is already being blocked for room "+ room
									return wxlanresult
							elif action == 'unblock':
								if r['enabled'] == True:
									payload = {
										"name": room,
										"order": 1,
										"src_wxtags": [i['id']],
										"dst_allow_wxtags": [],
										"dst_deny_wxtags": [],
										"blocked_apps": ["all-social"],
										"action": "block",
										"enabled": False
										}
									uri = "/api/v1/sites/"+ site_id +"/wxrules/"+ r['id']
									post = post_content(uri, payload, mist_api)
									if post['enabled'] == False:
										wxlanresult = "Social media rule was found and is now unblocked for room "+ room
										return wxlanresult
									else:
										wxlanresult = "Something went wrong error:2"
										return wxlanresult
								elif r['enabled'] == False:
									wxlanresult = "Social media is already unblocked for room "+ room
									return wxlanresult

				if action == 'block':
					payload = {
						"name": room,
						"order": 1,
						"src_wxtags": [i['id']],
						"dst_allow_wxtags": [],
						"dst_deny_wxtags": [],
						"blocked_apps": ["all-social"],
						"action": "block",
						"enabled": True
						}
					uri = "/api/v1/sites/"+ site_id +"/wxrules"
					post = post_content(uri, payload, mist_api)
					if post['enabled'] == True:
						wxlanresult = "Social media is now blocked for room "+ room
					else:
						wxlanresult = "Something went wrong error:3" 

				elif action == 'unblock':
					payload = {
						"name": room,
						"order": 1,
						"src_wxtags": [i['id']],
						"dst_allow_wxtags": [],
						"dst_deny_wxtags": [],
						"blocked_apps": ["all-social"],
						"action": "block",
						"enabled": False
						}
					uri = "/api/v1/sites/"+ site_id +"/wxrules"
					post = post_content(uri, payload, mist_api)
					if post['enabled'] == False:
						wxlanresult = "Social media is now unblocked for room "+ room
					else:
						wxlanresult = "Something went wrong error:4"
						
			else:
				wxlanresult = "Sorry, "+ room +" does not exist. Please check it is correct and try again."

	return wxlanresult


if __name__ == "__main__":
	try:
		action = sys.argv[1]
		room = sys.argv[2]
	except IndexError:
		print ("Usage: classroom_social.py <block/unblock> <room>")
		sys.exit(1)

	if action == 'check':
		for site in get_content(mist_sites, mist_api):
			site_id = site["id"]
			check_rule = get_content("/api/v1/sites/"+ site_id +"/wxrules", mist_api)
			#print (check_rule)
			for r in check_rule:
				if 'name' in r:
					if (r['name'] == room):
						if r['enabled'] == False:
							#print ("Rule enabled")
							sys.exit(1)
					else:
						#print ("Rule for this room not found")
						sys.exit(1)


			print ("Rule is enabled.")
			sys.exit(0)

	response = wxlan_api(room, mist_token, action, mist_sites, mist_api)
	print (response)